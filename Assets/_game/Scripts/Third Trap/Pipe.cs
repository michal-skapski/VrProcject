using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour
{
    public void SetMaterial(Material material)
    {
        if(gameObject.GetComponent<Renderer>() != null)
        {
            gameObject.GetComponent<Renderer>().material = material;
        }
        else
        {
            //List<GameObject> list = new List<GameObject>();
            foreach(Transform child in transform) 
            {
                child.gameObject.GetComponent<Renderer>().material = material;
            }

        }
    }
}
