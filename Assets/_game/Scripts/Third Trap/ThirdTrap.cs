using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdTrap : Trap
{
    [SerializeField] private int _correctPosition;
    public int _position;
    public bool _isInteracted = false;

    private void UpdatePosition()
    {
        transform.Rotate(new Vector3(0f, 0f, 90f)); //((float)_position / 4) * 360
    }

    public void Interact()
    {
        if(!_isInteracted) 
        {
            if (_position == 4)
            {
                _position = 1;
            }
            else
            {
                _position++;
            }
            UpdatePosition();
            _isInteracted = true;
        }      
    }

    public void StopInteraction()
    {
        _isInteracted = false;
    }

    private void Start()
    {
        //_position = 4;
        if(_correctPosition > 4)
        {
            _correctPosition = 4;
        }
        if(_correctPosition < 1) 
        {
            _correctPosition = 1;
        }
        //UpdatePosition();
    }

    private void Update()
    {
        if (_position == _correctPosition && !_trapCompleted)
        {
            _trapCompleted = true;
        }
        else if (_position != _correctPosition) 
        {
            _trapCompleted = false;
        }
    }
}
