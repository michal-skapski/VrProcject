using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerDisplay : MonoBehaviour
{
    [SerializeField] private Material _unconnectedMat, _connectedMat = null;
    [SerializeField] private List<Pipe> _pipes = new List<Pipe>();
    [SerializeField] private List<Trap> _trapElements = new List<Trap>();

    private void Update()
    {
        for(int i = 0; i < _trapElements.Count; i++)
        {
            if(_trapElements[i].TrapCompleted )
            {
                _pipes[i].SetMaterial(_connectedMat);
            }
            else
            {
                _pipes[i].SetMaterial(_unconnectedMat);
            }
        }
    }
}
