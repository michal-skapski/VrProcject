using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

public class PlayerDeath : MonoBehaviour
{
    public bool playerPassTheTrial = false;

    private const float _playerKillTime = 5f;

    private float _fogKillPos = 1.2f;

    [SerializeField] private GameObject _fogObj = null;
    [SerializeField] private CountdownTimer _countdownTimer = null;
    [SerializeField] private Image _killImage = null;
    private const string _startSceneName = "FirstTrap";
    public void DeathCheck()
    {
        if(playerPassTheTrial == false)
        {
            StartCoroutine(ReleaseGas());
        }
    }
    private IEnumerator ReleaseGas()
    {
        _fogObj.transform.DOMoveY(_fogKillPos, _playerKillTime, false);
        _killImage.gameObject.SetActive(true);
        _countdownTimer.DeleteSave();
        yield return new WaitForSeconds(_playerKillTime);
        SceneManager.LoadScene(_startSceneName);
    }
}
