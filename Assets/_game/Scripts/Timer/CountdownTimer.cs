using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountdownTimer : MonoBehaviour
{
    [SerializeField] private List<TextMeshProUGUI> _wallTimers = new List<TextMeshProUGUI>();

    #region Timers
    private int _oneSecond = 1;
    private int _oneMinute = 1;
    private int _sixtySeconds = 60;
    private int _displayMinutes = 0;
    private int _displaySeconds = 0;
    private int _tenSeconds = 10;
    #endregion
    public int remainingTime = 0;

    private string _displayedString;

    [SerializeField] private PlayerDeath _playerDeath = null;

    private const string _remainingTime = "Remaining Time";
    private const string _remainingMinutes = "Remaining Minutes";

    private const string _timerZero = "0";
    private const string _timerColon = ":";
    private const string _timerColonZero = ":0";

    private void LoadData()
    {
        if (PlayerPrefs.HasKey(_remainingTime))
        {
            remainingTime = PlayerPrefs.GetInt(_remainingTime);
        }
    }
    public void StartTheTrap()
    {
        LoadData();
        StartCoroutine(CountdownStart());
        Mathf.Round(_displayMinutes = remainingTime / _sixtySeconds);
    }

    public IEnumerator CountdownStart()
    {
        do
        {
            yield return new WaitForSeconds(_oneSecond);
            Timer();
        } while (remainingTime > 0f);
        _playerDeath.DeathCheck();
        yield return null;
    }
    public void SaveTime() // use in the level progression
    {
        PlayerPrefs.SetInt(_remainingTime, remainingTime);
        PlayerPrefs.Save();
    }
    public void DeleteSave()
    {
        PlayerPrefs.DeleteAll();
    }
    private void OnApplicationQuit()
    {
        DeleteSave();
    }
    private void Timer()
    {
        if (_displaySeconds == 0)
        {
            _displayMinutes -= _oneMinute;
            _displaySeconds = _sixtySeconds;
        }
        _displaySeconds--;
        for (int i = 0; i < _wallTimers.Count; i++)
        {
            if (_displaySeconds < _tenSeconds)
            {
                _displayedString = (_displayMinutes + _timerColonZero + _displaySeconds);
            }
            else
            {
                _displayedString = (_timerZero + _displayMinutes + _timerColon + _displaySeconds);
            }

            _wallTimers[i].text = _displayedString;
        }
        remainingTime--;
    }
}
