using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class PlayerKidnapped : MonoBehaviour
{
    [SerializeField] private GameObject _levelEnviorement = null;
    [SerializeField] private Transform _destination = null;
    [SerializeField] private float _kidnappingDuration;
    [SerializeField] private Trap _levelLoadTrigger;

    private int _firstTrapScene = 1;

    [SerializeField] private AudioSource _audio = null;
    private void PlaySound()
    {
        _audio.Play();
    }

    private void Start()
    {
        Kidnapped();
    }
    public void Kidnapped()
    {

        _levelEnviorement.transform.DOMove(_destination.position, _kidnappingDuration);
        StartCoroutine(WaitForMovement());
    }

    IEnumerator WaitForMovement()
    {
        yield return new WaitForSeconds(_kidnappingDuration);
        PlaySound();
        _levelLoadTrigger.TrapCompleted = true;
    }
}
