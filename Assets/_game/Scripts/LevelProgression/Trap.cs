using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    protected bool _trapCompleted;
    public bool TrapCompleted    { get { return _trapCompleted; } set { _trapCompleted = value; } }

    public void CompletedLevel()
    {
        _trapCompleted = true;
    }
}
