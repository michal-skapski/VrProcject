using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class LevelProgressionManager : MonoBehaviour
{
    [SerializeField] private List<Trap> _traps = new List<Trap>();
    [SerializeField] private EndLevelDoor _endLevelDoor = null;
    [SerializeField] private GameObject _fadeOutImage = null;
    [SerializeField] private float _fadeOutDuration;

    private bool IsLevelCompleted()
    {
        foreach (Trap trap in _traps) 
        {
            if(trap.TrapCompleted == false)
            {
                return false;
            }           
        }
        return true;
    }

    private void Update()
    {
        if (IsLevelCompleted()) 
        {
            StartCoroutine(WaitForFadeOut());
        }
    }

    IEnumerator WaitForFadeOut()
    {
        _fadeOutImage.SetActive(true);
        yield return new WaitForSeconds(_fadeOutDuration);
        _endLevelDoor.LoadNextLevel();
    }
}
