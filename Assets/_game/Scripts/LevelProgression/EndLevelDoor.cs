using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndLevelDoor : MonoBehaviour
{
    [SerializeField] private int _nextSceneID;
    [SerializeField] private CountdownTimer _countdownTimer = null;
    public void LoadNextLevel()
    {
        if (_countdownTimer != null)
        {
            _countdownTimer.SaveTime();
        }
        SceneManager.LoadScene(_nextSceneID);
    }
}
