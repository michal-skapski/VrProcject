using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DropPipe : MonoBehaviour
{
    private const float _half = 2f;
    private const float _secureTime = 0.3f;
    [SerializeField] private float _animTime = 0f;
    [SerializeField] private GameObject _endObj = null;

    private const string _sawTag = "Saw";
    private const string _printerTag = "Printer";
    private const string _cleanBottleTag = "CleanBottle";
    private int _correctItems = 0;
    private int _incorrectItems = 0;
    [SerializeField] private int _maxItems = 3;
    [SerializeField] private Trap _trap = null;
    [SerializeField] private PlayerDeath _playerDeathManager = null;
    [SerializeField] private AudioSource _laughSound = null;
    private float _laughSoundLong = 2f;
    private void OnTriggerEnter(Collider other) // change the attached obj
    {
        if(other.GetComponent<PickableObj>() != null)
        {
            PickableObj obj = other.GetComponent<PickableObj>();
            if(obj.CompareTag(_sawTag) || obj.CompareTag(_printerTag) || obj.CompareTag(_cleanBottleTag))
            {
                obj.GetComponent<Collider>().enabled = false;
                _correctItems++;
                CheckProgress();
            }
            else
            {
                StartCoroutine(PlayLaugh);
                obj.GetComponent<Collider>().enabled = false;
                _incorrectItems++;
                CheckProgress();
            }
        }
    }
    public void DoMove(GameObject p_obj)
    {
        p_obj.GetComponent<Transform>();
        p_obj.transform.DOScale(p_obj.transform.localScale / _half, _animTime);
        p_obj.transform.DOMove(_endObj.transform.position, _animTime);
    }

    public IEnumerator PlayLaugh
    {
        get
        {
            if (!_laughSound.isPlaying)
            {
                _laughSound.Play();
            }
            yield return new WaitForSeconds(_laughSoundLong);

        }
    }

    private void CheckProgress()
    {
        if(_correctItems == _maxItems)
        {
            _trap.TrapCompleted = true;
        }
        else if(_incorrectItems == _maxItems)
        {
            _playerDeathManager.DeathCheck();
        }
    }
    public IEnumerator DestroyTheObj(GameObject p_thrownObj)
    {
        yield return new WaitForSeconds(_animTime + _secureTime); // wait for animation to end
        Destroy(p_thrownObj);
    }
}