using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropCollider : MonoBehaviour
{
    [SerializeField] private DropPipe _dropPipe = null;
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PickableObj>() != null)
        {
            PickableObj obj = other.GetComponent<PickableObj>();
            StartCoroutine(_dropPipe.DestroyTheObj(obj.gameObject));
            _dropPipe.DoMove(obj.gameObject);
        }
    }
}
