using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissCollider : MonoBehaviour
{
    [SerializeField] private DropPipe _dropPipe = null;
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PickableObj>() != null)
        {
            StartCoroutine(_dropPipe.PlayLaugh);
        }
    }
}
