using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKidnapping : MonoBehaviour
{
    [SerializeField] private Trap _trap;
    [SerializeField] private float _waitToKidnap = 30f;

    private AudioSource _kidnapSound;
    private void Awake()
    {
        _kidnapSound = GetComponentInChildren<AudioSource>();
    }
    void Start()
    {
        StartCoroutine(CountToKidnap()); // kidnap player if he don't press the button
    }
    private IEnumerator CountToKidnap()
    {
        yield return new WaitForSeconds(_waitToKidnap);
        PlayerKidnap();
    }
    private void PlayerKidnap()
    {
        if (_trap.TrapCompleted == false) 
        {
            _trap.CompletedLevel();
            _kidnapSound.Play();
        }
    }
}
