using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SubtitleDisplay : MonoBehaviour
{
    [SerializeField] private List<Subtitle> _subtitles = new List<Subtitle>();
    [SerializeField] private TextMeshProUGUI _textField = null;
    [SerializeField] private CountdownTimer _countdownTimer = null;

    [SerializeField] private List<AudioSource> _audio = new List<AudioSource>();
    private int i;

    [SerializeField] private float _lastDelay;

    private const string _emptyTimerText = "";

    private void Start()
    {
        StartCoroutine(WaitForNewDisplay());
        i = 0;
    }

    IEnumerator WaitForNewDisplay()
    {
        foreach (Subtitle sub in _subtitles) 
        {
            yield return new WaitForSeconds(sub.displayDelay);
            if (_audio[i]!= null)
            {
                _audio[i].Play();
                i++;
            }
            DisplayText(sub.text);
        }
        yield return new WaitForSeconds(_lastDelay);
        _countdownTimer.StartTheTrap();
        DisplayText(_emptyTimerText);
    }

    private void DisplayText(string text)
    {
        _textField.text = text;
    }
}
