using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Subtitle : MonoBehaviour
{
    [SerializeField] private float _displayDelay;
    [SerializeField] private string _text;

    public float displayDelay { get { return _displayDelay; } set { _displayDelay = value; } }
    public string text { get { return _text; } set { _text = value; } }
}
