using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BicycleAnimation : MonoBehaviour
{
    [SerializeField] private float _animTime = 0f;

    private Vector3 _doAnimPosition = new Vector3(0.93f, 0.513f, 26f);
    private Vector3 _doAnimRotation = Vector3.zero;

    private Animator _animator;
    private void Start()
    {
        StartCoroutine(DisableAnimator());
        transform.DOMove(_doAnimPosition, _animTime);
        transform.DOLocalRotate(_doAnimRotation, _animTime);
        _animator = GetComponent<Animator>();

    }
    private IEnumerator DisableAnimator()
    {
        yield return new WaitForSeconds(_animTime);
        _animator.enabled = false;
    }
}