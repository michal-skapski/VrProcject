using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSound : MonoBehaviour
{
    private AudioSource _audio = null;
    private void Awake()
    {
        _audio = GetComponent<AudioSource>();
    }
    public void PlayButtonSound()
    {
        if (_audio != null) 
        {
            _audio.Play();
        }
    }
}
