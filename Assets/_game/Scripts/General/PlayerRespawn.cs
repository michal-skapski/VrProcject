using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    [SerializeField] private GameObject _playerObj = null;
    public Vector3 playerRespawnPos;
    private void Start()
    {
        playerRespawnPos = _playerObj.transform.position;
    }
    public void ChangeSpawnPosition(Vector3 p_newSpawnPos)
    {
        playerRespawnPos = p_newSpawnPos;
    }
    public void RespawnPlayer()
    {
        _playerObj.transform.position = playerRespawnPos;
    }
}