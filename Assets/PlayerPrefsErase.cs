using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsErase : MonoBehaviour
{
    void Start()
    {
        PlayerPrefs.DeleteAll();
    }
}
